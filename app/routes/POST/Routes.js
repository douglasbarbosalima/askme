const Manager = require('./../../controllers/ClientController');

module.exports = application => {

//POST endpoint's
	application.post('/client/insert', (req, res) => {
		application.app.controllers.ClientController.insertClient(application, req, res);
	});
	application.post('/insert/address', (req, res) => {
		application.app.controllers.ClientController.insertAddress(application, req, res);
	});
	application.post('/client/upload', (req, res) => {
		application.app.controllers.ClientController.uploadFile(application, req, res);
	});
}

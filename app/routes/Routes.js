module.exports = application => {

	//Default endpoint
	application.use((req, res) => {
		return res.status(404).send({
				status: 404,
				message: 'Error! Route not found!'
		});
	});
}

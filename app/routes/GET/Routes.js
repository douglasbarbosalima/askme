const Manager = require('./../../controllers/ClientController');

module.exports = application => {

	//GET endpoint's
	application.get('/', (req, res) => {
		application.app.controllers.ClientController.init(application, req, res);
	});
	application.get('/client/list/all', (req, res) => {
		application.app.controllers.ClientController.listAllClients(application, req, res);
	});
	application.get('/client/list/:id', (req, res) => {
		application.app.controllers.ClientController.listClientById(application, req, res);
	});
	application.get('/address/list/all', (req, res) => {
		application.app.controllers.ClientController.listAllAddresses(application, req, res);
	});
	application.get('/address/list/:id', (req, res) => {
		application.app.controllers.ClientController.listAddressById(application, req, res);
	});

}

const Manager = require('./../../controllers/ClientController');

module.exports = application => {

  //PUT endpoint's
	application.put('/client/edit', (req, res) => {
		application.app.controllers.ClientController.editClient(application, req, res);
	});
	application.put('/address/edit', (req, res) => {
		application.app.controllers.ClientController.editAddress(application, req, res);
	});
}

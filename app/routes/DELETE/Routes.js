const Manager = require('./../../controllers/ClientController');

module.exports = application => {

	//DELETE endpoint's
	application.delete('/client/remove', (req, res) => {
		application.app.controllers.ClientController.removeClient(application, req, res);
	});
	application.delete('/address/remove', (req, res) => {
		application.app.controllers.ClientController.removeAddress(application, req, res);
	});
}

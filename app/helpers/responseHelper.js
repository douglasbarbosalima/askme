class ResponseHelper {

  constructor(status, message, response){
    this._status = status;
    this._message = message;
    this._response = response;
  }

  getResponse(){
    this._response.status(this._status);
    this._response.send({
      status: this._status,
      message: this._message
    });
  }
}

module.exports = function() {
  return ResponseHelper;
}

class ClientDAO {

	constructor(connection) {

		this._connection = connection();
	}

	listAllClients(){

		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL listar_clientes()',
				[],
				(err, result) => {
					if(err) reject(err);

					resolve(result[0]);
				}
			);
		});

	}

	listClientById(idClient){

		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL listar_clientes_por_id(?)',
				[idClient],
				(err, result) => {
					if(err) reject(err);

					resolve(result[0]);
				}
			);
		});
	}

	listAllAddresses(){

		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL listar_enderecos()',
				[],
				(err, result) => {
					if(err) reject(err);

					resolve(result[0]);
				}
			);
		});
	}

	listAddressById(idAddress){

		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL listar_enderecos_por_id(?)',
				[idAddress],
				(err, result) => {
					if(err) reject(err);

					resolve(result[0]);
				}
			);
		});
	}

	insertClient(...args){
		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL inserir_cliente(?)',
				[...args],
				(err, result) => {
					if(err) reject(err);

					resolve(result[0][0].idCliente);
					return;
				}
			);
		});
	}

	insertAddress(...args){

		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL inserir_endereco(?)',
				[...args],
				(err, result) => {
					if(err) reject(err);

					resolve(result[0]);
				}
			);
		});
	}

	editClient(...args){

		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL editar_clientes(?)',
				[...args],
				(err, result) => {
					if(err) reject(err);

					resolve(result);
				}
			);
		});
	}

	editAddress(...args){

		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL editar_enderecos(?)',
				[...args],
				(err, result) => {
					if(err) reject(err);

					resolve(result);
				}
			);
		});
	}

	removeClient(idClient){

		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL remover_cliente(?)',
				[idClient],
				(err, result) => {
					if(err) reject(err);

					resolve(result);
				}
			);
		});
	}

	removeAddress(idAddress){

		return new Promise((resolve, reject) => {
			this._connection.query(
				'CALL remover_endereco(?)',
				[idAddress],
				(err, result) => {
					if(err) reject(err);

					resolve(result);
				}
			);
		});
	}

	listAddressByClientId(clientId){

		return new Promise( (resolve, reject) => {
			this._connection.query(
				'CALL listar_enderecos_por_id_cliente(?)',
				[clientId],
				(err, result) => {
					if(err) reject(err);

					resolve(result[0]);
				}
			)
		});
	}

}

module.exports = function(){
	return ClientDAO;
}

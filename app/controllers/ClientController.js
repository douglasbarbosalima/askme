const ClientDAO  = require('./../dao/ClientDAO')();
const Helper = require('./../helpers/GenericHelper');
const ResponseHelper = require('./../helpers/responseHelper')();
const pool = require('./../../config/dbConnection');
const excel2Json = require('my-xls-to-json');
const moment = require('moment');
moment.locale('pt-BR');

module.exports.init = async (application, req, res) => {

	const handleResponse = new ResponseHelper(200, 'Hello, World', res);
	handleResponse.getResponse();
}

module.exports.listAllClients = async (application, req, res) => {

	const DAO = new ClientDAO(pool);
	const clients = await DAO.listAllClients();

	if(!clients){
		let handleResponse = new ResponseHelper(500, `There's a error on list Clients`, res);
		handleResponse.getResponse();

		return;
	}

	let objClient = [];
	let clientLength = clients.length - 1;
	clients.forEach( async (data, index) => {

		data.dataNascimentoCliente = moment(data.dataNascimentoCliente).format('LLL');
		data.dataInsercaoCliente = moment(data.dataInsercaoCliente).format('LLL');
		data.addresses = await DAO.listAddressByClientId(data.idCliente);
		objClient.push(data);

		if(index === clientLength){

			const handleResponse = new ResponseHelper(200, objClient, res);
			handleResponse.getResponse();
		}
	});

}

module.exports.listClientById = async (application, req, res) => {

	const { id } = req.params;
	const DAO = new ClientDAO(pool);
	const client = await DAO.listClientById();

	if(!client){
		let handleResponse = new ResponseHelper(500, `There's a error on list Client`, res);
		handleResponse.getResponse();

		return;
	}

	client[0].dataNascimentoCliente = moment(client[0].dataNascimentoCliente).format('LLL');
	client[0].dataInsercaoCliente = moment(client[0].dataInsercaoCliente).format('LLL');
	client[0].address = await DAO.listAddressByClientId(id);

	const handleResponse = new ResponseHelper(200, client[0], res);
	handleResponse.getResponse();
}

module.exports.listAllAddresses = async (application, req, res) => {

	const DAO = new ClientDAO(pool);
	let addresses = await DAO.listAllAddresses();

	if(!addresses) {
		let handlerResponse = new ResponseHelper(500, 'The list of addresses could not been listed', res);
		handlerResponse.getResponse();
	}

	let handlerResponse = new ResponseHelper(200, addresses, res);
	handlerResponse.getResponse();
}

module.exports.listAddressById = async (application, req, res) => {

	const { id } = req.params;
	const DAO = new ClientDAO(pool);
	let address = await DAO.listAddressById(id);

	if(!address) {
		let handlerResponse = new ResponseHelper(500, 'The list of addresses could not been listed', res);
		handlerResponse.getResponse();
	}

	let handlerResponse = new ResponseHelper(200, address, res);
	handlerResponse.getResponse();
}

module.exports.insertClient = async (application, req, res) => {

		const { name, surname, bornDate, cpf } = req.body;

		const DAO = new ClientDAO(pool);
		const inserted = await DAO.insertClient([
			name,
			surname,
			new Date(bornDate),
			cpf
		]);

		if(!inserted){
			let handleResponse = new ResponseHelper(500, 'The Client could not been inserted', res);
			handleResponse.gerResponse();
		}

		let handleResponse = new ResponseHelper(
			200,
			{
				idInserted: inserted
			},
			res
		);

		handleResponse.getResponse();
}

module.exports.insertAddress = async (application, req, res) => {

	const { name, complement, clientId } = req.body;

	const DAO = new ClientDAO(pool);
	const inserted = await DAO.insertAddress([
		name,
		complement,
		clientId
	]);

	if(!inserted){
		let handleResponse = new ResponseHelper(500, 'The Address could not been inserted', res);
		handleResponse.gerResponse();
	}

	let handleResponse = new ResponseHelper(
		200,
		{
			idInserted: inserted[0].idEndereco
		},
		res
	);

	handleResponse.getResponse();

}

//Note: For this method, import the born_date on american format. YYYY/mm/dd
module.exports.uploadFile = async (application, req, res) => {

	const { clients } = req.files;
	const filename = clients.name;

	const upload = await Helper.uploadFile(clients, filename);

	if(upload){
		setTimeout( () => {

			excel2Json(`app/assets/${filename}`, (err, output) => {

				const DAO = new ClientDAO(pool);

				if(output){

					//Here you insert the name of your sheet
					output["Planilha1"].forEach( async data => {

						let inserted = await DAO.insertClient([
							data.client_name,
							data.client_surname,
							new Date(data.client_born_date),
							data.client_cpf
						]);

						let addressInserted = await DAO.insertAddress([
							data.client_address,
							data.client_complement,
							inserted
						]);
					});

					let handleResponse = new ResponseHelper(200, 'The users has been insert', res);
					handleResponse.getResponse();
				}
				else {
					let handleResponse = new ResponseHelper(500, `There's a error on import list`, res);
					handleResponse.getResponse();
				}
			});
		}, 1000);
	}
	else {
		let handleResponse = new ResponseHelper(500, `There's a error on import list`, res);
		handleResponse.getResponse();
	}

}

module.exports.editClient = async (application, req, res) => {

	const { clientId, name, surname, bornDate, cpf } = req.body;

	const DAO = new ClientDAO(pool);
	const inserted = await DAO.editClient([
		clientId,
		name,
		surname,
		new Date(bornDate),
		cpf
	]);

	if(!inserted){
		let handleResponse = new ResponseHelper(500, 'The Client could not been updated', res);
		handleResponse.getResponse();
	}

	let handleResponse = new ResponseHelper(200, 'The Client Information has been updated', res);
	handleResponse.getResponse();
}

module.exports.editAddress = async (application, req, res) => {

	const { idAddress, name, complement } = req.body;

	const DAO = new ClientDAO(pool);
	const inserted = await DAO.editAddress([
		idAddress,
		name,
		complement
	]);

	if(!inserted){
		let handleResponse = new ResponseHelper(500, 'The Address could not been updated', res);
		handleResponse.gerResponse();
	}

	let handleResponse = new ResponseHelper(200, 'The Address Information has been updated', res);
	handleResponse.getResponse();
}

module.exports.removeClient = async (application, req, res) => {

	const { clientId } = req.body;

	const DAO = new ClientDAO(pool);
	const removed = await DAO.removeClient(clientId);

	if(!removed){
		let handleResponse = new ResponseHelper(500, 'The Client could not been removed', res);
		handleResponse.getResponse();
	}

	let handleResponse = new ResponseHelper(200, `The ClientID: ${clientId} has been removed`, res);
	handleResponse.getResponse();
}

module.exports.removeAddress = async (application, req, res) => {

	const { addressId } = req.body;

	const DAO = new ClientDAO(pool);
	const removed = await DAO.removeAddress(addressId);

	if(!removed){
		let handleResponse = new ResponseHelper(500, 'The Address could not been removed', res);
		handleResponse.getResponse();
	}

	let handleResponse = new ResponseHelper(200, `The AddressID: ${addressId} has been removed`, res);
	handleResponse.getResponse();
}

CREATE DATABASE  IF NOT EXISTS `askme` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `askme`;
-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: askme
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nome_cliente` varchar(100) NOT NULL,
  `sobrenome_cliente` varchar(150) NOT NULL,
  `data_nascimento_cliente` datetime NOT NULL,
  `cpf_cliente` varchar(50) NOT NULL,
  `data_insercao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Your name here','Your surname here','1995-08-28 00:00:00','12345678910','2019-02-23 20:32:56'),(2,'Douglas 2','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(3,'Douglas 3','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(4,'Douglas 14','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(5,'Douglas 17','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(7,'Douglas 19','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(8,'Douglas 9','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(9,'Douglas 1','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(10,'Douglas 5','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(11,'Douglas 4','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(12,'Douglas 15','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(13,'Douglas 13','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(14,'Douglas 6','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(15,'Douglas 7','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(16,'Douglas 10','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(17,'Douglas 11','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(18,'Douglas 18','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(19,'Douglas 22','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(20,'Douglas 21','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(21,'Douglas 23','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(22,'Douglas 16','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(23,'Douglas 12','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(24,'Douglas 20','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-23 20:32:56'),(25,'Douglas','Barbosa de Lima','1995-08-28 00:00:00','45963778808','2019-02-24 15:21:44'),(26,'Your name here','Your surname here','1995-08-28 00:00:00','12345678910','2019-02-24 16:05:06'),(28,'Your name here','Your surname here','1995-08-28 00:00:00','12345678910','2019-02-24 16:07:18'),(30,'Your name here','Your surname here','1995-08-28 00:00:00','12345678910','2019-02-24 16:08:33'),(38,'Your name here','Your surname here','1995-08-28 00:00:00','12345678910','2019-02-24 16:15:22'),(39,'Your name here','Your surname here','1995-08-28 00:00:00','12345678910','2019-02-24 16:22:58'),(40,'Your name here','Your surname here','1995-08-28 00:00:00','12345678910','2019-02-24 16:23:24');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enderecos`
--

DROP TABLE IF EXISTS `enderecos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enderecos` (
  `id_endereco` int(11) NOT NULL AUTO_INCREMENT,
  `nome_endereco` varchar(255) NOT NULL,
  `complemento_endereco` varchar(100) NOT NULL,
  `id_cliente` int(4) NOT NULL,
  `data_insercao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_endereco`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enderecos`
--

LOCK TABLES `enderecos` WRITE;
/*!40000 ALTER TABLE `enderecos` DISABLE KEYS */;
INSERT INTO `enderecos` VALUES (1,'Rua Teste 4','Casa 4',3,'2019-02-23 20:32:56'),(2,'Rua Teste 3','Casa 3',2,'2019-02-23 20:32:56'),(3,'Rua Teste 20','Casa 20',7,'2019-02-23 20:32:56'),(4,'Rua Teste 15','Casa 15',4,'2019-02-23 20:32:56'),(5,'Rua Teste 10','Casa 10',8,'2019-02-23 20:32:56'),(7,'Rua Teste 9','Casa 9',6,'2019-02-23 20:32:56'),(8,'Name of Address','Complement Address',1,'2019-02-23 20:32:56'),(9,'Rua Teste 6','Casa 6',10,'2019-02-23 20:32:56'),(10,'Rua Teste 5','Casa 5',11,'2019-02-23 20:32:56'),(11,'Rua Teste 14','Casa 14',13,'2019-02-23 20:32:56'),(12,'Rua Teste 16','Casa 16',12,'2019-02-23 20:32:56'),(13,'Rua Teste 7','Casa 7',14,'2019-02-23 20:32:56'),(14,'Rua Teste 8','Casa 8',15,'2019-02-23 20:32:56'),(15,'Rua Teste 11','Casa 11',16,'2019-02-23 20:32:56'),(16,'Rua Teste 12','Casa 12',17,'2019-02-23 20:32:56'),(17,'Rua Teste 19','Casa 19',18,'2019-02-23 20:32:56'),(18,'Rua Teste 22','Casa 22',20,'2019-02-23 20:32:56'),(19,'Rua Teste 23','Casa 23',19,'2019-02-23 20:32:56'),(20,'Rua Teste 17','Casa 17',22,'2019-02-23 20:32:56'),(21,'Rua Teste 24','Casa 24',21,'2019-02-23 20:32:56'),(22,'Rua Teste 21','Casa 21',24,'2019-02-23 20:32:56'),(23,'Rua Teste 13','Casa 13',23,'2019-02-23 20:32:56'),(24,'Rua Teste 2','Casa 2',9,'2019-02-23 20:32:56'),(25,'Rua Berrini','Apto. 1º andar. Nº 1478',1,'2019-02-23 21:01:23'),(26,'Avenida Paulista','Nº 1478, Prédio A, Torre B, Complemento: C.',1,'2019-02-24 15:23:51'),(28,'Avenida do Estado','Torre A.',1,'2019-02-24 15:30:35'),(29,'Avenida Paulista','Nº 1478, Prédio A, Torre B, Complemento: C.',25,'2019-02-24 15:35:40'),(30,'Name of Address','Complement Address',1,'2019-02-24 16:07:18'),(38,'Name of Address','Complement Address',1,'2019-02-24 16:22:58'),(39,'Name of Address','Complement Address',1,'2019-02-24 16:23:24');
/*!40000 ALTER TABLE `enderecos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'askme'
--
/*!50003 DROP PROCEDURE IF EXISTS `editar_clientes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `editar_clientes`(
	`idCliente` INT(4),
    `nomeCliente` VARCHAR(150),
    `sobrenomeCliente` VARCHAR(100),
    `dataNascimentoCliente` DATETIME,
    `cpfCliente` VARCHAR(50)
)
BEGIN
	UPDATE 
		`askme`.`clientes`
	SET
		`nome_cliente` = IF(!ISNULL(`nomeCliente`), `nomeCliente`, `nome_cliente`),
		`sobrenome_cliente` = IF(!ISNULL(`sobrenomeCliente`), `sobrenomeCliente`, `sobrenome_cliente`),
		`data_nascimento_cliente` = IF(!ISNULL(`dataNascimentoCliente`), `dataNascimentoCliente`, `data_nascimento_cliente`),
		`cpf_cliente` = IF(!ISNULL(`cpfCliente`), `cpfCliente`, `cpf_cliente`)
	WHERE 
		`id_cliente` = `idCliente`;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `editar_enderecos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `editar_enderecos`(
	`idEndereco` INT(4),
    `nomeEndereco` VARCHAR(150),
    `complementoEndereco` VARCHAR(100)
)
BEGIN
	UPDATE 
		`askme`.`enderecos`
	SET
		`nome_endereco` = IF(!ISNULL(`nomeEndereco`), `nomeEndereco`, `nome_endereco`),
		`complemento_endereco` = IF(!ISNULL(`complementoEndereco`), `complementoEndereco`, `complemento_endereco`)
	WHERE 
		`id_endereco` = `idEndereco`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `inserir_cliente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `inserir_cliente`(
	`nomeCliente` VARCHAR(150),
    `sobrenomeCliente` VARCHAR(200),
    `dataNascimentoCliente` DATETIME,
    `cpfCliente` VARCHAR(50)
)
BEGIN
	INSERT INTO `askme`.`clientes`
		(
			`nome_cliente`,
			`sobrenome_cliente`,
			`data_nascimento_cliente`,
			`cpf_cliente`
        )
	VALUES
		(
			`nomeCliente`,
            `sobrenomeCliente`,
            `dataNascimentoCliente`, 
            `cpfCliente`
        );
        
        SELECT LAST_INSERT_ID() AS 'idCliente';

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `inserir_endereco` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `inserir_endereco`(
	`nomeEndereco` VARCHAR(150),
    `complementoEndereco` VARCHAR(100),
    `idCliente` INT(4)
)
BEGIN
	INSERT INTO 
    `askme`.`enderecos`
		(
			`nome_endereco`,
			`complemento_endereco`,
			`id_cliente`
        )
	VALUES
		(
			`nomeEndereco`,
            `complementoEndereco`,
            `idCliente`
        );
        
        SELECT LAST_INSERT_ID() AS 'idEndereco';

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listar_clientes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `listar_clientes`()
BEGIN
	SELECT 
		`clientes`.`id_cliente` AS 'idCliente',
		`clientes`.`nome_cliente` AS 'nomeCliente',
		`clientes`.`sobrenome_cliente` AS 'sobrenomeCliente',
		`clientes`.`data_nascimento_cliente` AS 'dataNascimentoCliente',
		`clientes`.`cpf_cliente` AS 'cpfCliente',
        `clientes`.`data_insercao` AS 'dataInsercaoCliente'
	FROM 
		`askme`.`clientes`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listar_clientes_por_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `listar_clientes_por_id`(
	`idCliente` INT(4)
)
BEGIN
	SELECT 
		`clientes`.`id_cliente` AS 'idCliente',
		`clientes`.`nome_cliente` AS 'nomeCliente',
		`clientes`.`sobrenome_cliente` AS 'sobrenomeCliente',
		`clientes`.`data_nascimento_cliente` AS 'dataNascimentoCliente',
		`clientes`.`cpf_cliente` AS 'cpfCliente'
	FROM 
		`askme`.`clientes`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listar_enderecos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `listar_enderecos`()
BEGIN
	SELECT 
		`enderecos`.`id_endereco` AS 'idEndereco',
		`enderecos`.`nome_endereco` AS 'nomeEndereco',
		`enderecos`.`complemento_endereco` AS 'complementoEndereco',
		`enderecos`.`id_cliente` AS 'idCliente'
	FROM 
		`askme`.`enderecos`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listar_enderecos_por_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `listar_enderecos_por_id`(
	`idEndereco` INT(4)
)
BEGIN
	SELECT 
		`enderecos`.`id_endereco` AS 'idEndereco',
		`enderecos`.`nome_endereco` AS 'nomeEndereco',
		`enderecos`.`complemento_endereco` AS 'complementoEndereco',
		`enderecos`.`id_cliente` AS 'idCliente'
	FROM 
		`askme`.`enderecos`
	WHERE
		`enderecos`.`id_endereco` = `idEndereco`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listar_enderecos_por_id_cliente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `listar_enderecos_por_id_cliente`(
	`idCliente` INT(4)
)
BEGIN
	SELECT 
		`enderecos`.`id_endereco` AS 'idEndereco',
		`enderecos`.`nome_endereco` AS 'nomeEndereco',
		`enderecos`.`complemento_endereco` AS 'complementoEndereco',
		`enderecos`.`id_cliente` AS 'idCliente'
	FROM 
		`askme`.`enderecos`
	WHERE
		`enderecos`.`id_cliente` = `idCliente`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `remover_cliente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `remover_cliente`(
	`idCliente` INT(4)
)
BEGIN
	DELETE FROM 
		`askme`.`clientes`
	WHERE 
		`clientes`.`id_cliente` = `idCliente`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `remover_endereco` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`administrator`@`localhost` PROCEDURE `remover_endereco`(
	`idEndereco` INT(4)
)
BEGIN
	DELETE FROM 
		`askme`.`enderecos`
	WHERE 
		`id_endereco` = `idEndereco`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-24 17:28:56

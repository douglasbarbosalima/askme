//Import the config's from server.
const app = require('./config/server');

const moment = require('moment')();
moment.locale('pt-BR');

const chalk = require('chalk');
const port = 3000;

/* Paramter the listen Port*/
app.listen(port, function(){
	console.log(`Server is on: ${chalk.yellow(moment.format('LLL'))} ${chalk.green('✓')}`);
});

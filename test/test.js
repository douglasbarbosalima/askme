const chai = require('chai');
const chaiHttp = require('chai-http');
const assert = chai.assert;
const expect = chai.expect;
const should = chai.should();
const application = require('./../config/server.js');

chai.use(chaiHttp);

describe('Test', () => {

	it('Default Response of Application', done => {
		chai.request(application)
		.get('/')
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body.message).to.equal('Hello, World');
			done();
		});
	});

	it('List All Clients', done => {
		chai.request(application)
		.get('/client/list/all')
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res).to.not.equal(null || undefined);
			expect(res.body.message).to.be.an('array');
			expect(res.body.message[0]).to.have.property('idCliente');
			done();
		});
	});

	it('List All Address', done => {
		chai.request(application)
		.get('/address/list/all')
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res).to.not.equal(null || undefined);
			expect(res.body.message).to.be.an('array');
			expect(res.body.message[0]).to.have.property('idEndereco');
			done();
		});
	});

	it('List Client by ID', done => {
		chai.request(application)
		.get('/client/list/1')
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body.message).to.not.equal(null || undefined);
			expect(res.body.message).to.be.an('object');
			expect(res.body.message).to.have.property('idCliente');
			done();
		});
	});

	it('List Address by ID', done => {
		chai.request(application)
		.get('/address/list/1')
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res).to.not.equal(null || undefined);
			expect(res.body.message).to.be.an('array');
			expect(res.body.message[0]).to.have.property('idEndereco');
			done();
		});
	});

	it('Insert Client', (done) => {
		chai.request(application)
		.post('/client/insert')
		.type('form')
		.send({
			'name': 'Your name here',
			'surname': 'Your surname here',
			'bornDate': '1995/08/28',
			'cpf': '12345678910'
		})
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body).to.not.equal(null || undefined);
			expect(res.body.message).to.have.property('idInserted');
			done();
		});
	});

	it('Insert Address', (done) => {
		chai.request(application)
		.post('/insert/address')
		.type('form')
		.send({
			'name': 'Name of Address',
			'complement': 'Complement Address',
			'clientId': '1'
		})
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body).to.not.equal(null || undefined);
			expect(res.body.message).to.have.property('idInserted');
			done();
		});
	});

	it('Update Client', (done) => {
		chai.request(application)
		.put('/client/edit')
		.send({
			'clientId': 1,
			'name': 'Your name here',
			'surname': 'Your surname here',
			'bornDate': '1995/08/28',
			'cpf': '12345678910'
		})
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body).to.not.equal(null || undefined);
			expect(res.body.message).to.equal('The Client Information has been updated');
			done();
		});
	});

	it('Update Address', (done) => {
		chai.request(application)
		.put('/address/edit')
		.send({
			'idAddress': 8,
			'name': 'Name of Address',
			'complement': 'Complement Address'
		})
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body).to.not.equal(null || undefined);
			expect(res.body.message).to.equal('The Address Information has been updated');
			done();
		});
	});

	it('Remove Client by ID', done => {
		chai.request(application)
		.delete('/client/remove')
		.send( { 'clientId': 26 } )
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body).to.not.equal(null || undefined);
			done();
		});
	});

	it('Remove Client by ID', done => {
		chai.request(application)
		.delete('/address/remove')
		.send( { 'addressId': 26 } )
		.end((err, res) => {
			expect(res).to.have.status(200);
			expect(res.body).to.not.equal(null || undefined);
			done();
		});
	});



});

const express = require('express');
const consign = require('consign');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const expressValidator = require('express-validator');
const expressSession = require('express-session');

const app = express();

app.use(expressSession({
	secret: 'askme',
	resave: false,
	saveUnitialized: false
}));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(fileUpload());

app.use(function(err, req, res, next){
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Headers', '*');
	next();
});

app.use(expressValidator());

consign()
	.include('app/routes')
	.then('config/dbConnection.js')
	.then('app/dao')
	.then('app/controllers')
	.into(app);

module.exports = app;

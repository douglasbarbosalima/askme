require('dotenv').config();
const mysql = require('mysql');
const { DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_CONNECTION_LIMIT } = process.env;

const dbConfig = {
	host : DB_HOST,
	user : DB_USER,
	password : DB_PASS,
	database : DB_NAME,
	connectionLimit : DB_CONNECTION_LIMIT
};

const pool = mysql.createPool(dbConfig);

module.exports = function () {
	return pool;
}
